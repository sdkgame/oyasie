import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropDownAvatarComponent } from './drop-down-avatar.component';

describe('DropDownAvatarComponent', () => {
  let component: DropDownAvatarComponent;
  let fixture: ComponentFixture<DropDownAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropDownAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropDownAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
