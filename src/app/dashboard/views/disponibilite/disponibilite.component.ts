import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { DisponibilityService } from '../../services/disponibility.service';
import { Disponibilite, DispoPeriodDays, StatutPeriod, DispoPeriodYear, DispoPeriodMonth } from 'src/app/modele/disponibility';
import { AuthService } from 'src/app/auth/services/auth.service';
import { User } from 'src/app/modele/user';

@Component({
  selector: 'app-disponibilite',
  templateUrl: './disponibilite.component.html',
  styleUrls: ['./disponibilite.component.css']
})
export class DisponibiliteComponent implements OnInit,OnChanges {
  showDefModal:boolean=false;
  showUsedModal:boolean=false;
  showUnUsedModal:boolean=false;
  myDisponibilite:Disponibilite;
  disponibilityCalendar={};
  typeFlashMessage:string='info';
  messageFlashMessage:string="Welcome";
  showFlashMessage:boolean=false;
  durationFlashMessage:number=5000;
  dataForColorCalendar:any={};
  authUser:User;
  currentDate:any={};
  constructor(private disponibiliteService:DisponibilityService,private authService:AuthService) { }
  ngOnInit(): void {
    
    this.authUser=this.authService.getUser();
    // console.log(this.authUser);
    this.disponibiliteService.subcribeToUser().subscribe(dispo=>{
      if(dispo.email==this.authUser.email) 
      {
        this.myDisponibilite=dispo.disponibilite;
        this.authUser.setDispo(this.myDisponibilite);
        console.log(dispo);
        this.dataForColorCalendar=this.myDisponibilite.toObject()["disponibilite"];
      }
      
    });
    this.disponibiliteService.emitUserDisponibilite();
    
  }
  ngOnChanges(changes:SimpleChanges)
  {
    // console.log(changes);
  }
  shwitchToModal(state)
  {
    // console.log("State: ",state," showUsedModal ", this.showUsedModal, "showUnUnsedModal ",this.showUnUsedModal);
    if(state=="free" || state=="no_free") 
    {
      this.showUsedModal=true;
      this.showUnUsedModal=false;
    }
    else 
    {
      this.showUnUsedModal=true;
      this.showUsedModal=false;
    }
    // console.log("State: ",state," showUsedModal ", this.showUsedModal, "showUnUnsedModal ",this.showUnUsedModal);
  }
  handleCelClick(data)
  {
    this.currentDate=data;
    console.log(data);
    this.shwitchToModal(data.statut);
  }
  addDisponiblity(data)
  {
    // console.log(data);
    if(data.error.length>0)
    {
      this.messageFlashMessage='';
      data.error.forEach(element => {
        this.messageFlashMessage+=element;
      });
      this.typeFlashMessage='danger';
      this.durationFlashMessage=10000;      
    }
    else
    {
      let dispoDay=new DispoPeriodDays(this.currentDate.day,StatutPeriod.available,data.data.periods);

      let dispoDayO:Record<number,DispoPeriodDays>={};
      dispoDayO[this.currentDate.day]=dispoDay;
      let dispoMonth=new DispoPeriodMonth(this.currentDate.month,dispoDayO);

      let dispoMonthO:Record<string,DispoPeriodMonth>={};
      dispoMonthO[this.currentDate.month]=dispoMonth;
      let dispoYear=new DispoPeriodYear(this.currentDate.year,dispoMonthO);
      this.typeFlashMessage="success";
      this.messageFlashMessage="Opération d'ajout de disponibnilité réussit";
      this.disponibiliteService.addDisponibility(this.authUser,dispoYear)
      .catch(err=>{
        this.typeFlashMessage="danger";
        this.messageFlashMessage=err.message;
        console.log(err);
      });
      // this.myDisponibilite=this.disponibiliteService.getDisponibiliteOf(this.authUser);
      // console.log("dispo html",this.myDisponibilite.toObject());

      // this.dataForColorCalendar=this.myDisponibilite.toObject()["disponibilite"];
    }
    this.showFlashMessage=true;
    setTimeout(()=>this.showFlashMessage=false,this.durationFlashMessage);
  }
  hideUnUsedModal()
  {
    this.showUnUsedModal=false;
  }
  hideUsedModal()
  {
    this.showUsedModal=false;
  }
}
