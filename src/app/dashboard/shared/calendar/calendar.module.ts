import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DayCellComponent } from './day-cell/day-cell.component';
import { CalendarComponent } from './calendar.component';
@NgModule({
  declarations: [
    DayCellComponent,
    CalendarComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CalendarComponent
  ]
})
export class CalendarModule { }
