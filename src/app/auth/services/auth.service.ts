import { Injectable, OnDestroy } from '@angular/core';
import { User } from 'src/app/modele/user';
import { ManageAccountService } from 'src/app/services/manageAccount.service';
import { FetchApiService } from 'src/app/services/fetch-api.service';
import { serializeData, deSerializeData, keyExist, clearDataFromStorage } from 'src/app/utils/localStorageSerialization';
import { ActionStatut } from 'src/app/modele/statut';
import { FireBaseConstant } from 'src/app/shared/firebase-constant';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:User=null;
  isAuth:boolean=false;
  constructor(private manageAccount:ManageAccountService,private fetchApi:FetchApiService) {}
  handleApiError(result:any)
  {
    switch(result.apiCode)
    {
      case FireBaseConstant.AUTH_WRONG_PASSWORD:
        result.message="Email ou mot de passe incorrect";
        break;
      case FireBaseConstant.AUTH_WEAK_PASSWORD:
        result.message="Mot de passe doit avoir au moins 6 carracteres"
        break;
      case FireBaseConstant.AUTH_EMAIL_ALREADY_USE:
        result.message="Email déjà utiliser par un autre utilisateur";
        break;
      case FireBaseConstant.NET_NETWORK_FAIL:
        result.message="Hors connexion. Veuillez verifier votre connectivité réseau";
        break;          
    };
  }
  signIn(userN:User,shouldGetUserData:boolean=true):Promise<ActionStatut>
  {
    let action=new ActionStatut();
    return new Promise<ActionStatut>((resolve,reject)=>{
      this.fetchApi.signInApi(userN.email,userN.password)
      .then(result=>{
        this.isAuth=true;
        action=result;
        if(shouldGetUserData) return this.manageAccount.getUsersFromApi();
      })
      .then((result)=>{
        this.user=this.manageAccount.findByEmail(userN.email); 
        // console.log(this.user);
        //this.setUserDataToStorage();
        resolve(result);
      })  
      .catch(result=>{
        this.handleApiError(result);
        reject(result);
      })       
    });
  }

  signOut():void
  {
    this.user=null;
    this.isAuth=false;
    this.manageAccount.clearData();
    clearDataFromStorage();
    this.fetchApi.signOutApi();
  }
  isConnected():boolean
  {
    // this.getUserDataFromStorage();
    return this.isAuth && this.user!=null && this.user!=undefined;
  }
  getUser():User
  {
    return this.user;
  }
  signInNewUser(user:User):Promise<ActionStatut>
  {
    return new Promise<ActionStatut>((resolve,reject)=>
    {
      this.manageAccount.createAccount(user)
      .then((result)=>{
        return this.signIn(user,false);
      })
      .then((result)=>{
        this.user=User.fromObject({
          email:user.email,
          name:user.name,
          id:this.fetchApi.user.uid,
          disponibilite:{}
        });
        return this.manageAccount.saveAccount(this.user)
      })
      .then((e)=>{        
        this.isAuth=true;
        return this.manageAccount.getUsersFromApi();
      })
      .then((result)=>{
        // this.setUserDataToStorage();
        resolve(result);
      })
      .catch(e=>
      {
        this.handleApiError(e);
        reject(e)
      })
    });
  }
  private getUserDataFromStorage():void
  {
    if(this.isAuth || !keyExist("is_auth")) return;
    this.isAuth=deSerializeData("is_auth")==null?false:true;
    this.user=User.fromObject(deSerializeData("current_user"));

  }
  private setUserDataToStorage():void
  {
    console.log("User",this.isAuth,this.user);
    if(!this.isAuth) return;
    serializeData("is_auth",this.isAuth);
    serializeData("current_user",this.user);
  }
}
