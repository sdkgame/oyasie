import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBoxBodyComponent } from './modal-box-body.component';

describe('ModalBoxBodyComponent', () => {
  let component: ModalBoxBodyComponent;
  let fixture: ComponentFixture<ModalBoxBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBoxBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBoxBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
