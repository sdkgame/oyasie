import { NgModule } from '@angular/core';

import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './views/login/login.component';
import { NewAccountComponent } from './views/new-account/new-account.component';
import { SharedModule } from './shared/shared.module';
import { SharedModule as SharedExtenalModule } from 'src/app/shared/shared.module';

@NgModule({
    imports:[
        SharedModule,
        AuthRoutingModule,
        SharedExtenalModule
    ],
    declarations: [
        AuthComponent,
        LoginComponent,
        NewAccountComponent
    ],
    providers: []
})

export class AuthModule {}