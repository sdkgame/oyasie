import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipComponent } from './tooltip/tooltip.component';
import { VBarSpinnerComponent } from './v-bar-spinner/v-bar-spinner.component';

@NgModule({
  declarations: [TooltipComponent, VBarSpinnerComponent],
  imports: [
    CommonModule
  ],
  exports:[
    TooltipComponent,
    VBarSpinnerComponent
  ]
})
export class SharedModule { }
