import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-day-cell',
  templateUrl: './day-cell.component.html',
  styleUrls: ['./day-cell.component.css']
})
export class DayCellComponent implements OnInit {
  @Input() value:string;
  @Input() color:string;
  constructor() { }

  ngOnInit(): void {
    
  }

}
