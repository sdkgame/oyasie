import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardModuleRouting } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from './shared/shared.module';
import { ViewsModule } from './views/views.module';
import { DisponibilityService } from './services/disponibility.service';
import { NotificationService } from './services/notification.service';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardModuleRouting,
    SharedModule,
    ViewsModule
  ],
  providers:[
    DisponibilityService,
    NotificationService
  ]
})
export class DashboardModule { }
