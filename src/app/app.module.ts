import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/services/auth.service';
import { ManageAccountService } from './services/manageAccount.service';
import { DashboardModuleRouting } from './dashboard/dashboard-routing.module'

import { DashboardModule } from './dashboard/dashboard.module';
import { FetchApiService } from './services/fetch-api.service';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    DashboardModuleRouting,
    DashboardModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    ManageAccountService,
    FetchApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
