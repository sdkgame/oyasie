import { Component, OnInit , Input} from '@angular/core';

@Component({
  selector: 'app-v-bar-spinner',
  templateUrl: './v-bar-spinner.component.html',
  styleUrls: ['./v-bar-spinner.component.css']
})
export class VBarSpinnerComponent implements OnInit {

  @Input() width:string;
  @Input() height:string;
  constructor() { }

  ngOnInit(): void {
  }

}
