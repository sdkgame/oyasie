import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { DispoPeriodTime } from 'src/app/modele/disponibility';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActionStatut } from 'src/app/modele/statut';

@Component({
  selector: 'app-modal-box-used-period',
  templateUrl: './modal-box-used-period.component.html',
  styleUrls: ['./modal-box-used-period.component.css']
})
export class ModalBoxUsedPeriodComponent implements OnInit,OnChanges {
  @Input() show;
  @Output() eventDefinition=new EventEmitter<Object>();
  @Output() eventClose=new EventEmitter<Object>();
  form:FormGroup;
  periods:DispoPeriodTime[]=[];
  disponibility:any[]=[];
  dispoJourne:boolean=false;
  titleValue:FormControl=new FormControl('',Validators.required);
  constructor() { }

  ngOnInit(): void {
    let obj=this.newForm();
    
    let result={};
    result[`formControlStartTime${obj.random}`]=obj.formControlStartTime;
    result[`formControlEndTime${obj.random}`]=obj.formControlEndTime;
    this.form=new FormGroup({
      'title':this.titleValue,
      ...result
    });
  }
  ngOnChanges(changes:SimpleChanges)
  {
    console.log("used modal ",changes);
  }
  newForm():any
  {
    let obj={
      startTime:'',
      endTime:''
    }
    let randomE=Math.floor(Math.random()*100);
    obj[`formControlStartTime${randomE}`]=new FormControl(`${new Date().getHours()}:${new Date().getMinutes()}`);
    obj[`formControlEndTime${randomE}`]=new FormControl(`${new Date().getHours()}:${new Date().getMinutes()}`);
    obj['formControlStartTimeName']=`formControlStartTime${randomE}`;
    obj['formControlEndTimeName']=`formControlEndTime${randomE}`;
    this.disponibility.push(obj);
    return {
      'formControlStartTime':obj[`formControlStartTime${randomE}`],
      'formControlEndTime':obj[`formControlEndTime${randomE}`],
      'random':randomE
    };
  }
  addDisponibility()
  {
    let obj=this.newForm();
    this.form.addControl(`formControlStartTime${obj.random}`,obj.formControlStartTime);
    this.form.addControl(`formControlEndTime${obj.random}`,obj.formControlEndTime);
  }
  removeDisponibility()
  {
    if(this.disponibility.length==0) return;
    let result=this.disponibility.splice(this.disponibility.length-1,1);
    this.form.removeControl(result[0].formControlStartTimeName);
    this.form.removeControl(result[0].formControlEndTimeName);
  }
  close()
  {
    this.eventClose.emit();
  }
  clear()
  {
    this.titleValue.setValue("");
    this.disponibility=[];
    this.ngOnInit();
  }
  reinitialized()
  {
    this.dispoJourne=false;
    this.clear();
  }
  trackError(arr:any[]):string[]
  {
    arr.sort((a,b)=>{
       return DispoPeriodTime.isValidPeriod(a.endTime,b.startTime)?-1:1;
    });
    let error:string[]=[];
    for(let i=0; i<arr.length-1;i+=2)
    {
      if(!DispoPeriodTime.noContains(DispoPeriodTime.fromObject(arr[i]),DispoPeriodTime.fromObject(arr[i+1])))
      {
        error.push(`La periode ${arr[i].startTime}-${arr[i].endTime} et la periode ${arr[i+1].startTime}-${arr[i+1].endTime} se chevauche`);
      }
    }
    return error;
  }
  setDispoJournee()
  {
    this.dispoJourne=!this.dispoJourne;
    this.clear();
    console.log(this.dispoJourne);
  }
  submit()
  {
    let reg=new RegExp('^formControlEndTime([0-9]{2})$');
    let error:string[]=[];
    let arrData:any[]=[];
    // console.log("dispo: ",this.dispoJourne);
    if(this.dispoJourne)
    {
      this.periods.push(new DispoPeriodTime("00:00","24:00"));
    }
    else
    {
      for(let key in this.form.value)
      {
        let find=reg.exec(key);
        if(find)
        {
          try
          {
            this.periods.push(new DispoPeriodTime(this.form.value[`formControlStartTime${find[1]}`],this.form.value[find[0]]));
            arrData.push({
              startTime:this.form.value[`formControlStartTime${find[1]}`],
              endTime:this.form.value[find[0]]
            });
          } 
          catch(e)
          {
            error.push(`Impossible de prendre ${this.form.value[`formControlStartTime${find[1]}`]} comme heure de debut car elle est plus grande que ${this.form.value[find[0]]} etant l'heure de fin`);
          }
        }
      }
    }
    this.clear();
    this.eventDefinition.emit({data:this.periods,'error':[...error,...this.trackError(arrData)]});
  }
}
