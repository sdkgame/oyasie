import { Injectable } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { User } from 'src/app/modele/user';
import { Notification } from 'src/app/modele/notification';
import { AuthService } from 'src/app/auth/services/auth.service';
import { FetchApiService } from 'src/app/services/fetch-api.service';
import { ManageAccountService } from 'src/app/services/manageAccount.service';
import { ActionStatut } from 'src/app/modele/statut';
import { FireBaseConstant } from 'src/app/shared/firebase-constant';
@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  managerAccountSuscription:Subscription;
  userNotif:any;
  constructor(private authService:AuthService,private managerAccount:ManageAccountService,
     private fetchApiService:FetchApiService) {
    this.userNotif=authService.getUser();
   }
   getAllUserNotifications():Notification[]
   {
    let result:Notification[]=[];
    result=this.userNotif.notifications.lues.map((notif)=>Notification.fromObject(notif));
    this.userNotif.notifications.non_lues.forEach(notif => result.push(Notification.fromObject(notif)));
    return result;
  }
  sendNewNotification(notif:Notification):Promise<ActionStatut>
  {
    return new Promise<ActionStatut>((resolve,reject)=>{
      this.fetchApiService.add(`users/${this.managerAccount.findByEmail(notif.receiverEmail).id}/notifications/non_lues`,notif.toObject())
      .then(result=>{
        resolve(result)})
      .catch(error=>{
        switch(error.apiCode)
        {
          case FireBaseConstant.DATABASE_NETWORK_ERROR:
          case FireBaseConstant.NET_NETWORK_FAIL:
            error.message="Erreur de réseau. veuillez vérifier votre connexion internet";
            break
          case FireBaseConstant.DATABASE_OPERATION_FAILED:
            error.message="Echec de l'opération. Une erreur inconnue l'ors de l'opération"
            break;
        };
        reject(error);
      })
    });    
  }
  markAllNonReadAsRead()
  {
    
  }
}
