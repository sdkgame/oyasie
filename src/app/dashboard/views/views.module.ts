import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { SharedModule } from './../shared/shared.module';
import { DisponibiliteComponent } from './disponibilite/disponibilite.component';
import { RechercherComponent } from './rechercher/rechercher.component';




@NgModule({
  declarations: [
    HomeComponent, 
    DisponibiliteComponent, 
    RechercherComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports:[
    HomeComponent,
    DisponibiliteComponent,
    RechercherComponent
  ]
})
export class ViewsModule { }
