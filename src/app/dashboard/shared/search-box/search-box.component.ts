import { Component,EventEmitter, OnInit,Input, Output, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit,OnChanges {
  users:any[];
  @Input() data:any[];
  @Output() eventSelectedUser=new EventEmitter<Object>();
  input:FormControl=new FormControl('')
  constructor() { }

  ngOnInit(): void {
    this.users=[...this.data];
  }
  ngOnChanges(changes:SimpleChanges)
  {
    // console.log(changes)
    if(changes.hasOwnProperty("data")) this.users=[...this.data];
  }
  selectUser(id):void
  {
    this.eventSelectedUser.emit(id);
  }
  submit(event):void
  {
    let value=this.input.value;
    if(value.length<1)
    {
      this.users=[...this.data];
      return;
    }
    this.users=this.data.filter(obj=>{
      let pos=obj.name.toLowerCase().indexOf(value);
      if(pos!==-1) return true;
      return false; 
    });    
  }
}
