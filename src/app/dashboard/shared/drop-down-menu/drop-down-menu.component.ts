import { Component, OnInit, Input,Output} from '@angular/core';

@Component({
  selector: 'app-drop-down-menu',
  templateUrl: './drop-down-menu.component.html',
  styleUrls: ['./drop-down-menu.component.css']
})
export class DropDownMenuComponent implements OnInit {
 
  @Input() item:any[]=[
    {
      link:'/notification/non_lues/1',
      text:'notificaton 1',
      icon:''
    },
    {
      link:'/notification/non_lues/1',
      text:'notificaton 2',
      icon:'',
      has_top_bar:true
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
