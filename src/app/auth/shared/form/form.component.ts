import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input() childs: any[];
  @Input() textBtn: string;
  @Output() submitEvent=new EventEmitter<Object>();
  @Input() textError:string;
  @Input() textWaiting:string;
  form:FormGroup;
  submitted:boolean=false;
  waiting:boolean=false;
  constructor() {}

  ngOnInit(): void {
    let obj={};
    this.childs.forEach(elt=>{
      if(elt.validators===undefined) obj[`${elt.name}`]=new FormControl('');
      else obj[`${elt.name}`]=new FormControl('',[...elt.validators]);

    });
    this.form=new FormGroup(obj);
  }
  get f()
  {
    return this.form.controls;
  }
  control(name:string)
  {
    return eval(`this.f.${name}`);
  }
  submit()
  {
    this.submitted=true;
    this.waiting=true;
    this.submitEvent.emit(this.form.value);
    this.waiting=false;
    //this.childs.forEach(elt=>console.log(`${elt.name} : ${this.form.value[elt.name]}`));
  }

}
