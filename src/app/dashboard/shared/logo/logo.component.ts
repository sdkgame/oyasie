import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css']
})
export class LogoComponent implements OnInit {
  @Input() src:string;
  @Input() alt:string;
  @Input() width:string;
  @Input() height:string;
  @Input() nameApp:string;
  constructor() { }

  ngOnInit(): void {
  }

}
