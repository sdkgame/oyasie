import { Component, OnInit } from '@angular/core';
import { ManageAccountService } from 'src/app/services/manageAccount.service';
import { DisponibilityService } from '../../services/disponibility.service';
import { User } from 'src/app/modele/user';
import { dateAccess } from 'src/app/utils/dateAccess';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Notification } from 'src/app/modele/notification';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-rechercher',
  templateUrl: './rechercher.component.html',
  styleUrls: ['./rechercher.component.css']
})
export class RechercherComponent implements OnInit {
  dataUser:any[]=[];
  curentUser:User=null;
  showAskDisponibilityModal:boolean=false;
  dataForColorCalendar:any={};
  typeFlashMessage:string;
  messageFlashMessage:string;
  durationFlashMessage:number=3000;
  showFlashMessage:boolean=false;
  dataForAskDisponibility=[];
  constructor(private manageAccount:ManageAccountService,
    private dispoService:DisponibilityService,
    private authService:AuthService,
    private notifService:NotificationService
    ) { }

  ngOnInit(): void {
    // console.log(this.manageAccount.getUserFromApi());
    this.dataUser=this.manageAccount.getUsers().filter(user=>this.authService.getUser().email!=user.email);
    this.curentUser=null;    
  }
  handleCelClick(date)
  {
    if(this.curentUser==null)
    {
      //on recherche la liste des personnes etant libre a cette date
      this.dataUser=this.dispoService.findFreeUserTo(new Date(date.year,dateAccess.getPositionOfMonth(date.month),parseInt(date.day)));
    }
    else
    {
      //ici on fait une demande de service (en envoyant une notification a l'utilisateur concerné)
      if(date.statut=="free")
      {
        this.showAskDisponibilityModal=true;
        // console.log(this.curentUser.disponibilite.toObject().disponibilite[date.year][date.month][date.day]);
        this.dataForAskDisponibility=this.curentUser.disponibilite.toObject().disponibilite[date.year][date.month][date.day];
      }
    }
  }
  handleUserClick(data)
  {
    if(this.curentUser!=null && data==this.curentUser.id) return;
    this.curentUser=this.manageAccount.findById(data);
    this.dataForColorCalendar=this.dispoService.getDisponibiliteOf(this.curentUser).toObject()["disponibilite"];
  }
  addNotification(data)
  {
    if(data.error.length>0)
    {
      this.messageFlashMessage='';
      data.error.forEach(element => {
        this.messageFlashMessage+=element;
      });
      this.typeFlashMessage='danger';         
      this.showFlashMessage=true;
      setTimeout(()=>this.showFlashMessage=false,this.durationFlashMessage);   
    }
    else
    {
      // console.log("add notif ", data);
      let notifs:Notification=new Notification(this.authService.getUser().email,this.curentUser.email,`Vous avez une demande de disponibilité avec ce message: ${data.data.message}`);
      
      notifs.disponibilite=data.data.periods.map(horaire => `Debut: ${horaire.startTime} Fin:${horaire.endTime}`);
      this.typeFlashMessage="success";      
      this.messageFlashMessage="Demande de service envoyé avec success";
      this.notifService.sendNewNotification(notifs)
      .then((result)=>{
        this.showFlashMessage=true;
        setTimeout(()=>this.showFlashMessage=false,this.durationFlashMessage);   
      })
      .catch((error)=> this.messageFlashMessage=error.message)
      .then(()=>{
        this.showFlashMessage=true;
        setTimeout(()=>this.showFlashMessage=false,this.durationFlashMessage);   
      });

    }
  }
  hideModal()
  {
    this.showAskDisponibilityModal=false;
  }
}
