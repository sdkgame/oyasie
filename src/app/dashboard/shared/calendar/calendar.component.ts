import { Component, OnInit, Input ,Output, EventEmitter, ViewChild,ElementRef, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  @Output() eventSelect=new EventEmitter<Object>();
  @ViewChild('calendarComponent') calendarComponent:ElementRef;
  @Input() data:any={};
  dataColor:any={};
  days:any[];
  daysLab:string[]=['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche'];
  months:string[]=['Janvier','Fevrier','Mars','Avril','May','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'];
  currentDay:Date=new Date();
  currentMonth:Date;
  constructor() {}

  ngOnInit(): void {
    this.currentDay.setHours(0,0,0,0);     
    this.loadDaysList();
  }
  ngOnChanges(changes: SimpleChanges)
  {
    // console.log("changes");
    // console.log(changes);
    if(changes.hasOwnProperty('data'))
    {
      this.loadDaysList();
    }
  }
  handleCellClick(e)
  {
    let value=e.target.textContent;

    let statut="undefined";
    if(this.data.hasOwnProperty(this.currentMonth.getFullYear()) &&
      this.data[this.currentMonth.getFullYear()].hasOwnProperty(this.getCurrentMonth().toLowerCase()) &&
      this.data[this.currentMonth.getFullYear()][this.getCurrentMonth().toLowerCase()].hasOwnProperty(value)
    )
    {
      statut=(this.data[this.currentMonth.getFullYear()][this.getCurrentMonth().toLowerCase()][value].status)?"free":"no_free";
    }
    this.eventSelect.emit({
      year:this.currentDay.getFullYear(),
      month:this.getCurrentMonth().toLowerCase(),
      day:value,
      statut
    });
  }
  nextWeek():void
  {
    this.currentDay.setMonth(this.currentDay.getMonth()+1);
    this.loadDaysList();
  }
  previousWeek():void
  {
    this.currentDay.setMonth(this.currentDay.getMonth()-1);
    this.loadDaysList();
  }
  loadDaysList()
  {    
    this.days=[];
    this.currentMonth=new Date(this.currentDay.getFullYear(),this.currentDay.getMonth(),1)   
    let position=this.currentMonth.getDay()-1;
    // console.log(position);
    this.loadColorForDays();
    for(let i=0; i<position;i++) this.days.push({value:0,statut:'undefined'});
    let val=1;
    let maxDay:number=new Date(this.currentMonth.getFullYear(),this.currentMonth.getMonth()+1,0).getDate();
    for(let i=0;i<maxDay;i++)
    {
      if(val in this.dataColor) this.days.push({value:val,statut:this.dataColor[val]});
      else this.days.push({value:val,statut:''});
      val++; 
    }
    // console.log(maxDay);
    //console.log(val+position-1);
    let i=val+position;
    while(this.days.length%7!=0)
    {
      this.days.push({value:0,statut:''});
      i++;
      //console.log(i);
    }
  }
  getCurrentMonth()
  {
    return this.months[ this.currentMonth.getMonth()];
  }
  getNumberOfRow()
  {
    return this.days.length%7==0?Math.floor(this.days.length/7):Math.floor(this.days.length/7)+1;
  }
  showCalendar()
  {
    // console.log("test");
    // console.log(this.calendarComponent)
    this.calendarComponent.nativeElement.style.visibility="visible";
    this.calendarComponent.nativeElement.click();
  }
  loadColorForDays()
  {
    // console.log(this.data);
    if(!this.data.hasOwnProperty(this.currentMonth.getFullYear()) ||
    !this.data[this.currentMonth.getFullYear()].hasOwnProperty(this.months[this.currentMonth.getMonth()].toLowerCase()))
    {
      this.dataColor={};
      return;
    }
    let dataDispo=this.data[this.currentMonth.getFullYear()][this.months[this.currentMonth.getMonth()].toLowerCase()];
    for( let k in dataDispo)
    {
      this.dataColor[k]=dataDispo[k]["status"]==true?'free':'no_free';
    }
  }
}
