import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './views/home/home.component';
import { DisponibiliteComponent } from './views/disponibilite/disponibilite.component';
import { RechercherComponent } from './views/rechercher/rechercher.component';
import { GuardAuthService } from './services/guard-auth.service';
const routes = [
    { 
        path: 'dashboard',
        canActivate: [GuardAuthService],
        component: DashboardComponent,
        children: [
            { path: '', redirectTo:'signin',pathMatch: 'full'},
            { path:'home', component: HomeComponent,pathMatch: 'full' },
            { path:'disponibilite',component: DisponibiliteComponent},
            { path: 'rechercher', component: RechercherComponent}
            
        ]
    }    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardModuleRouting {}