import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drop-down-avatar',
  templateUrl: './drop-down-avatar.component.html',
  styleUrls: ['./drop-down-avatar.component.css','./dropDown.css']
})
export class DropDownAvatarComponent implements OnInit {
  showingVerticalNavLink:boolean=false;
  constructor() { }

  ngOnInit(): void {
  }
  handleClick =(e)=>
  {
      let elmt=e.target.nextElementSibling;
      console.log(elmt);
      if(this.showingVerticalNavLink)
      {
          //close vertical nav
          this.showingVerticalNavLink=false;
          elmt.classList.remove('itemLinkNavWithTransition');
          document.body.style.backgroundColor="white";

      }
      else
      {
          //show vertical nav
          this.showingVerticalNavLink=true;
          elmt.classList.add('itemLinkNavWithTransition');
          document.body.style.backgroundColor="rgba(0,0,0,0.4)";

      }
  }

}
