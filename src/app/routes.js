
export const Routes={
    signin:{
        name:'signin',
        url:'/signin'
    },
    signup:{
        name:'signup',
        url:'/signup'
    },
    dashboard:{
        name:'dashboard',
        home:{
            name:'home',
            url:'/dashboard/home'
        },
        disponibilite:{
            name:'disponibilite',
            url:'/dashboard/disponibilite'
        },
        rechercher:{
            name:'rechercher',
            url:'/dashboard/rechercher'
        }
    }
}