// `.eslintrc.js
module.exports = {
    parser: '@typescript-eslint/parser', // Parse TypeScript
    parserOptions: {
      project: './tsconfig.json',
      jsx: false // True for React
    },
  
    rules: { 
      /* disable or configure individual rules */
  
      /* Will need the following for React hooks: */
      // "react-hooks/rules-of-hooks": "error",
      // "react-hooks/exhaustive-deps": "warn"
    },
  
    // Use the rules from these plugins 
    extends: [
      'plugin:@typescript-eslint/recommended',
      'prettier/@typescript-eslint',
      'prettier',
      'plugin:prettier/recommended',
      'plugin:sonarjs/recommended',
      // 'plugin:react/recommended' // If we need React
    ]
  };