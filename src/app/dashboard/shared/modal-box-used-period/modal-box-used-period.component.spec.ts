import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBoxUsedPeriodComponent } from './modal-box-used-period.component';

describe('ModalBoxUsedPeriodComponent', () => {
  let component: ModalBoxUsedPeriodComponent;
  let fixture: ComponentFixture<ModalBoxUsedPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBoxUsedPeriodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBoxUsedPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
