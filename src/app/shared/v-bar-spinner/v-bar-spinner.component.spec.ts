import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VBarSpinnerComponent } from './v-bar-spinner.component';

describe('VBarSpinnerComponent', () => {
  let component: VBarSpinnerComponent;
  let fixture: ComponentFixture<VBarSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VBarSpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VBarSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
