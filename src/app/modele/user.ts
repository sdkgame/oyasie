import { Disponibilite } from './disponibility';

export class User {
    name:string;
    email:string;
    password:string;
    id:string;
    disponibilite:Disponibilite;
    constructor(email:string='',password:string='',name:string='',id:string='')
    {
        this.name=name;
        this.email=email;
        this.password=password;
        this.id=id;
    }
    setDispo(dispo:Disponibilite):void
    {
        this.disponibilite=dispo;
    }
    getDispo():Disponibilite
    {   
        return this.disponibilite;
    }
    toObject()
    {
        let obj= {
            name:this.name,
            email:this.email,
            password:this.password,
            id:this.id
        };
        return {...obj,...this.disponibilite.toObject()};
    }
    static fromObject(obj:any):User
    {
        let newUser= new User(obj.email?obj.email:'',obj.password?obj.password:'',obj.name?obj.name:'',obj.id?obj.id:'');
        newUser.setDispo(obj.disponibilite?Disponibilite.fromObject(obj.disponibilite):null);
        return newUser;
    }
    purgeEmail():string
    {
        let reg=/\.|#|,|;|!|\?|\[|\]|$/;
        return this.email.split(reg).join();
    }
    
}
