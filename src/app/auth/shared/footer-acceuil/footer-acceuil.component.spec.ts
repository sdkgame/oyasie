import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterAcceuilComponent } from './footer-acceuil.component';

describe('FooterAcceuilComponent', () => {
  let component: FooterAcceuilComponent;
  let fixture: ComponentFixture<FooterAcceuilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterAcceuilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterAcceuilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
