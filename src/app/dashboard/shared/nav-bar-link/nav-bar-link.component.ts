import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nav-bar-link',
  templateUrl: './nav-bar-link.component.html',
  styleUrls: ['./nav-bar-link.component.css']
})
export class NavBarLinkComponent implements OnInit {
  @Input() data:any[];
 
  constructor() { }

  ngOnInit(): void {
  }

  
}
