import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-drop-down-avatar-content',
  templateUrl: './drop-down-avatar-content.component.html',
  styleUrls: ['./drop-down-avatar-content.component.css']
})
export class DropDownAvatarContentComponent implements OnInit {
  @Input() className:string;
  @Input() image:string;
  @Input() data;
  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit(): void {
  }
  logout()
  {
    console.log("logout");
    this.authService.signOut();
    this.router.navigate(['/signin']);
  }
}
