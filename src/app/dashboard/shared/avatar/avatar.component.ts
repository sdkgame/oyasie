import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.css']
})
export class AvatarComponent implements OnInit {
  @Input() image:string;
  @Input() className:string;
  @Input() alt:string;
  @Input() width:string;
  @Input() height:string; 
  constructor() { }

  ngOnInit(): void {
  }

}
