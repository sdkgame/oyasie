import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  data:any[]=[
    {
        value:'Acceuil',
        link:'/dashboard/home'
    },
    {
        value:'Ma disponibilité',
        link:'/dashboard/disponibilite'
    },
    {
        value:'Rechercher',
        link:'/dashboard/rechercher'
    }
];

  constructor() { }

  ngOnInit(): void {
  }

}
