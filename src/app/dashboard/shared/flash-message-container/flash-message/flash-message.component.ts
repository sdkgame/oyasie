import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-flash-message',
  templateUrl: './flash-message.component.html',
  styleUrls: ['./flash-message.component.css']
})
export class FlashMessageComponent implements OnInit {
  @Input() content:string;
  @Input() type:string='info';
  @Input() duration:number=5000;
  @Input() show:boolean=false;
  @Input() styles;
  @ViewChild('flashMessage') flashMessage:ElementRef;
  constructor() { }

  ngOnInit(): void {
    // setTimeout(()=> this.hideMessage(),this.duration);
  }
  // ngOnChanges(changes:SimpleChanges):void
  // {
  //   console.log(changes.show);
  //   if(changes.show!==undefined && changes.show.firstChange)
  //   {
  //     console.log("du nouveau dans show");
  //     this.showMessage();
  //   }
  // }
  // hideMessage()
  // {
  //   // this.flashMessage.nativeElement.style.display="none";
  //   this.show
  // }
  // showMessage()
  // {
  //   this.flashMessage.nativeElement.style.display="block";
  //   setTimeout(()=> this.hideMessage(),this.duration);
  // }

}
