import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NavAccueilComponent } from './nav-accueil/nav-accueil.component';
import { FooterAcceuilComponent } from './footer-acceuil/footer-acceuil.component';
import { FormComponent } from './form/form.component';
import { SliderComponent } from './slider/slider.component';
import { SharedModule as SharedExtenalModule } from 'src/app/shared/shared.module';


@NgModule({
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule,
        SharedExtenalModule
    ],
    declarations:[
        NavAccueilComponent,
        FooterAcceuilComponent,
        FormComponent,
        SliderComponent
    ],
    exports: [
        NavAccueilComponent,
        FooterAcceuilComponent,
        FormComponent,
        SliderComponent,
        SharedExtenalModule
    ]
})
export class SharedModule{};
