import { Component, OnInit } from '@angular/core';
import {Validators } from '@angular/forms';
import { User } from 'src/app/modele/user';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css']
})
export class NewAccountComponent implements OnInit {
  childs : any[]=[
    {
      id:'userName',
      icon:'<i class="fa fa-user"></i>',
      type:'text',
      placeholder:'Nom',
      name:'nom',
      validators:[Validators.required,Validators.minLength(4)]
    },
    {
      id:'emailNewAccount',
      icon:'@',
      type:'email',
      placeholder:'Email',
      class:'second-input-group',
      name:'email',
      validators:[Validators.required,Validators.email]
    },
    {
      id:'password',
      icon:'<i class="fa fa-key"></i>',
      type:'password',
      placeholder:'Mot de passe',
      class:'second-input-group',
      name:'password',
      Validators:[Validators.required,Validators.minLength(6)]
    },
    {
      id:'confirmPassword',
      icon:'<i class="fa fa-key"></i>',
      type:'password',
      placeholder:'Confirmer mot de passe',
      class:'second-input-group',
      name:'confirmPassword',
      Validators:[Validators.required,Validators.minLength(6)]
    }
  ];
  textBtn:string="Creer";
  textError:string="";
  textWaiting:string="Veuillez patientez...";
  constructor(private authService: AuthService,private router:Router) { }

  ngOnInit(): void {
  }
  submit(data)
  {
    this.textError="Veuillez patienter...";
    if(data.password!==data.confirmPassword) this.textError="Veuillez saisie  un mot de passe identique";
    else
    {
      this.authService.signInNewUser(new User(data.email,data.password,data.nom))
      .then((e)=>{
        console.log(e);
        this.router.navigate(['dashboard/home']);
      })
      .catch(e=>{
        this.textError=e.message
        console.log(e);
      });
    }    
  }
}
