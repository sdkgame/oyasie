import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropDownAvatarContentComponent } from './drop-down-avatar-content.component';

describe('DropDownAvatarContentComponent', () => {
  let component: DropDownAvatarContentComponent;
  let fixture: ComponentFixture<DropDownAvatarContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropDownAvatarContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropDownAvatarContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
