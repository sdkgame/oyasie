import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

import { User } from '../modele/user';
import { ActionStatut } from '../modele/statut';
@Injectable({
  providedIn: 'root'
})
export class FetchApiService {

  firebaseConfig = {
    apiKey: "AIzaSyC35t8gc8Q0xY9OLJ8Nlqj-Yok7mdZ5UYA",
    authDomain: "oyasie-49ca7.firebaseapp.com",
    databaseURL: "https://oyasie-49ca7.firebaseio.com",
    projectId: "oyasie-49ca7",
    storageBucket: "oyasie-49ca7.appspot.com",
    messagingSenderId: "915231080440",
    appId: "1:915231080440:web:55a3fb7765e6eadb60200d",
    measurementId: "G-FR2N44PFSR"
  };
  debug:boolean=false;
  offlineMode:boolean=false;
  db:any;

  constructor(private httpClient:HttpClient) { 
    
    // Initialize Firebase
    firebase.initializeApp(this.firebaseConfig);
    this.db=firebase.database();
    this.setDebugMode();
    this.setModeApp();
  }
  setDebugMode()
  {
    // if(this.debug) firebase.firestore.setLogLevel('debug');
    
  }
  setModeApp()
  {
    // if(this.offlineMode) firebase.firestore().enablePersistence();
  }
  add(collectionName:string,value:any):ActionStatut
  {
    let action=new ActionStatut();
    let arrUrl=collectionName.split('/');
    this.db.collection(arrUrl[0]).doc(arrUrl[arrUrl.length-1]).set(value).then((doc)=>{
      action.description="successful add new collection with id "+doc.id;
    }).catch((err)=>{
      action.code=ActionStatut.UNKNOW_ERROR;
      action.message="error";
      action.description="Description of error: "+err;
    });
    return action;
  }
  set(url:string,value:any):ActionStatut
  {
    
    let action=new ActionStatut();
    this.db.ref(url).set(value).then((doc)=>{
      action.message="success";
      action.description="successful set new collection with id "+doc.id;
    }).catch((err)=>{
      action.code=ActionStatut.UNKNOW_ERROR;
      action.message="error";
      action.description="Description of error: "+err;
    });
    return action;
  }
  
  fetch(url:string):ActionStatut
  {
    let action=new ActionStatut();
    // let arrUrl=url.split('/');
    this.db.doc(url).get().then((doc)=>{
      if(doc.exists) 
      {
        action.description="Successful fetching information";
        action.result=doc.data();
      }
      else{
        action.code=ActionStatut.RESSOURCE_NOT_FOUND_ERROR;
        action.description=`Entity with url ${url} does not exist`;
        action.message="error";
      }
    }).catch(err=>{
      action.code=ActionStatut.UNKNOW_ERROR;
      action.message="error";
      action.description=`Description of error: ${err}`;
    });
    return action;
  }
  
  update(url:string,updates:any):any
  {
    this.db.doc(url).update(updates)
  }
  delete(url:string)
  {
    this.db.doc(url).remove();
  }
  get user()
  {
    return firebase.auth().currentUser;
  }
  signInApi(email:string,password:string):ActionStatut
  {
    let result:ActionStatut=new ActionStatut();
    
      firebase.auth().signInWithEmailAndPassword(email,password).then(()=>{
        result.description="Authentification successful";
      }).catch(error=>{
        result.code=ActionStatut.UNKNOW_ERROR;
        result.message="error";
        result.description=`Description of error: ${error}`;
      });
    return result;
  }
  signOutApi()
  { 
    firebase.auth().signOut();
  } 
  createUserApi(email:string,password:string):ActionStatut
  {
    let result:ActionStatut=new ActionStatut();
    firebase.auth().createUserWithEmailAndPassword(email,password).catch(error=>{
      result.code=ActionStatut.UNKNOW_ERROR;
      result.message=`error: ${error.code}`;
      result.description=`Description of error: ${error.message}`;
    });
    return result;
  }
}
