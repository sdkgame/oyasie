import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './views/login/login.component';
import { NewAccountComponent } from './views/new-account/new-account.component';
// import { FormComponent } from './shared/form/form.component';
const routes = [
    { 
        path: '',
        component: AuthComponent,
        children: [
            { path:'', redirectTo: 'signin', pathMatch: 'full' },
            { path:'signin', component: LoginComponent },
            { path: 'signup', component: NewAccountComponent }

        ]
    }    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule {}