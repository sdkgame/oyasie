import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBoxFooterComponent } from './modal-box-footer.component';

describe('ModalBoxFooterComponent', () => {
  let component: ModalBoxFooterComponent;
  let fixture: ComponentFixture<ModalBoxFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBoxFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBoxFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
