import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { LogoComponent } from './logo/logo.component';
import { NavBarLinkComponent } from './nav-bar-link/nav-bar-link.component';
import { DropDownAvatarComponent } from './drop-down-avatar/drop-down-avatar.component';
import { AvatarComponent } from './avatar/avatar.component';
import { DropDownAvatarContentComponent } from './drop-down-avatar-content/drop-down-avatar-content.component';
import { RouterModule } from '@angular/router';
import { CalendarModule } from './calendar/calendar.module';
import { SearchBoxComponent } from './search-box/search-box.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalBoxModule } from './modal-box/modal-box.module';
import { ModalBoxUnusedPeriodComponent } from './modal-box-unused-period/modal-box-unused-period.component';
import { FlashMessageContainerModule } from './flash-message-container/flash-message-container.module';
import { ModalBoxUsedPeriodComponent } from './modal-box-used-period/modal-box-used-period.component';
import { ModalBoxAskPeriodComponent } from './modal-box-ask-period/modal-box-ask-period.component';
import { DropDownMenuComponent } from './drop-down-menu/drop-down-menu.component';



@NgModule({
  declarations: [
    NavComponent,
    FooterComponent, 
    LogoComponent, 
    NavBarLinkComponent, 
    DropDownAvatarComponent, 
    AvatarComponent, 
    DropDownAvatarContentComponent,
    SearchBoxComponent,
    ModalBoxUnusedPeriodComponent,
    ModalBoxUsedPeriodComponent,
    ModalBoxAskPeriodComponent,
    DropDownMenuComponent    
  ],
  imports: [
    CommonModule,
    RouterModule,
    CalendarModule,
    ReactiveFormsModule,
    FormsModule,
    ModalBoxModule,
    FlashMessageContainerModule
  ],
  exports: [
    NavComponent,
    FooterComponent,
    RouterModule,
    LogoComponent,
    CalendarModule,
    SearchBoxComponent,
    ModalBoxModule,
    ModalBoxUnusedPeriodComponent,
    ModalBoxModule,
    FlashMessageContainerModule,
    ModalBoxUsedPeriodComponent,
    ModalBoxAskPeriodComponent,
    DropDownMenuComponent
  ]
})
export class SharedModule { }
