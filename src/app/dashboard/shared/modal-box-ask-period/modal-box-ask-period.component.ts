import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Form,FormGroup ,FormControl, Validators } from '@angular/forms';
import { DispoPeriodTime, StatutPeriod } from 'src/app/modele/disponibility';

@Component({
  selector: 'app-modal-box-ask-period',
  templateUrl: './modal-box-ask-period.component.html',
  styleUrls: ['./modal-box-ask-period.component.css']
})
export class ModalBoxAskPeriodComponent implements OnInit, OnChanges {
  @Input() show;
  @Input() dataDispo:any={};
  @Output() eventDefinition=new EventEmitter<Object>();
  @Output() eventClose=new EventEmitter<Object>();

  horaires:any[]=[];
  reservedDispo:any[]=[];
  labMessage:string="";
  titleValue:FormControl=new FormControl('',Validators.required);
  form:FormGroup;
  constructor() { }
  ngOnInit(): void {
    this.form=new FormGroup({
      'message':this.titleValue    
    });
  }
  ngOnChanges(changes:SimpleChanges)
  {
    // console.log('unused change ', changes);
    if(changes.hasOwnProperty('show'))
    {
      if(!changes.show.currentValue && !changes.show.firstChange)
      {
        this.ngOnInit();
      }
    }
    if(changes.hasOwnProperty('dataDispo'))
    {
      this.horaires=changes.dataDispo.currentValue.horaires;
      if(this.horaires) this.labMessage=this.horaires.length>0?this.horaires[0].message:'';
      this.reservedDispo=[];
    }
  }
  askDisponibility(e)
  {
    // console.log(e);
    // debugger;
    let i=this.findReservedDisponibility(this.horaires[parseInt(e.currentTarget.id)]);
    if(i>-1)
    {
      this.reservedDispo.splice(i,1);
    }
    else{
      this.reservedDispo.push(this.horaires[parseInt(e.currentTarget.id)]);
    }
    
  }
  findReservedDisponibility(dispo:any)
  {
    return this.reservedDispo.findIndex((elt)=>elt.startTime==dispo.startTime && elt.endTime==dispo.endTime);
  }
  clear()
  {
    this.titleValue.setValue("");
    this.ngOnInit();
  }
  close()
  {
    this.clear();
    this.eventClose.emit();
  }
  submit()
  {
    console.log(this.form.value);
    let arrData:DispoPeriodTime[]=[];
    let error:string[]=[];
    // console.log("dispo: ",this.dispoJourne);
    if(this.reservedDispo.length==0) 
      error.push(`Veuillez spécifier au moins une horaire de disponibilité`);
    this.reservedDispo.forEach(horaire=>{
      arrData.push(new DispoPeriodTime(horaire.startTime,horaire.endTime,StatutPeriod.unavailable,this.form.value.message))
    });   
    this.eventDefinition.emit({
      data:{
        periods:arrData,
        message:this.form.value.message
      },
      error
    });
  }
}
