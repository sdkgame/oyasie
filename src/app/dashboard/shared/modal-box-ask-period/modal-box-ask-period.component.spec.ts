import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBoxAskPeriodComponent } from './modal-box-ask-period.component';

describe('ModalBoxAskPeriodComponent', () => {
  let component: ModalBoxAskPeriodComponent;
  let fixture: ComponentFixture<ModalBoxAskPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBoxAskPeriodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBoxAskPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
