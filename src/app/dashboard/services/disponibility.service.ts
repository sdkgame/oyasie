import { Injectable } from '@angular/core';
import { FetchApiService } from 'src/app/services/fetch-api.service';
import { ManageAccountService } from 'src/app/services/manageAccount.service';
import { Subscription, Subject, defer } from 'rxjs';
import { filter } from 'rxjs/operators';
import { User } from 'src/app/modele/user';
import { Disponibilite, DispoPeriodYear, DispoPeriodDays } from 'src/app/modele/disponibility';
import { dateAccess } from 'src/app/utils/dateAccess';
import { ActionStatut } from 'src/app/modele/statut';
import { FireBaseConstant } from 'src/app/shared/firebase-constant';

@Injectable({
  providedIn: 'root'
})
export class DisponibilityService {
  userDispoData:any[];
  userSubjectTo=new Subject<any>();
  managerAccountSuscription:Subscription;

  constructor(private fetchApi:FetchApiService, private manageAccount:ManageAccountService) { 

    this.managerAccountSuscription=this.manageAccount.usersSubject.subscribe((userData)=> this.userDispoData=userData);
    this.manageAccount.emitUser();
    
  }
  subcribeToUser()
  {
    // console.log(this.userSubjectTo.pipe(defer(us=>us.email==user.email)));
    return this.userSubjectTo;
  }
  emitUserDisponibilite()
  {
    this.userDispoData.forEach(obj=>{
      this.userSubjectTo.next(User.fromObject(obj));
    });
  }
  getDisponibiliteOf(user:User):Disponibilite
  {
    let pos=this.manageAccount.findPositionByEmail(user.email);
    if(pos<0) return null;
    return Disponibilite.fromObject(this.userDispoData[pos].disponibilite)
  }

  addDisponibility(user:User,dispo:DispoPeriodYear):Promise<ActionStatut>
  {
    return new Promise<ActionStatut>((resolve,reject)=>{
      let pos=this.manageAccount.findPositionByEmail(user.email);
      if(this.userDispoData[pos].disponibilite==undefined) this.userDispoData[pos].disponibilite={};
      this.recurAddDisponibility(this.userDispoData[pos].disponibilite,dispo.toObject());
      this.fetchApi.set(`users/${this.fetchApi.user.uid}/disponibilite`,this.userDispoData[pos].disponibilite)
      .then(result=>{
        this.emitUserDisponibilite();
        resolve(result);
      })
      .catch(error=>{
        switch(error.apiCod)
        {
          case FireBaseConstant.NET_NETWORK_FAIL:
          case FireBaseConstant.DATABASE_NETWORK_ERROR:
          case FireBaseConstant.DATABASE_DISCONNECTED:
            error.message="Erreur réseau: Veuillez verifier votre connectivité réseau";
            break
          case FireBaseConstant.DATABASE_OPERATION_FAILED:
          case FireBaseConstant.DATABASE_PERMISSION_DENIED:
            error.message="Echec de l'opération: Une erreur inconnu est survenu";
            break;
          case FireBaseConstant.DATABASE_SERVICE_UNAVAILABLE:
            error.message="Echec de l'opération: Service indisponible. réesayer plus tard";
            break;
        };
        reject(error);
      });
    });    
  }
  recurAddDisponibility(objLevel,obj)
  {
    if(objLevel.hasOwnProperty(Object.keys(obj)[0]))
    {
      this.recurAddDisponibility(objLevel[Object.keys(obj)[0]],obj[Object.keys(obj)[0]]);
    }
    else{
      objLevel[Object.keys(obj)[0]]=obj[Object.keys(obj)[0]];
    }
  }
  findFreeUserTo(date:Date):User[]
  {
    let result:User[]=[];
    this.userDispoData.forEach(user=>{
      if(user.disponibilite.hasOwnProperty(date.getFullYear()) &&
      user.disponibilite[date.getFullYear()].hasOwnProperty(dateAccess.getMonthFromPosition(date.getMonth())) &&
      user.disponibilite[date.getFullYear()][dateAccess.getMonthFromPosition(date.getMonth())].hasOwnProperty(date.getDay())) 
      {
        let perioObj={};
        perioObj[date.getDay()]=user.disponibilite[date.getFullYear()][dateAccess.getMonthFromPosition(date.getMonth())][date.getDay()];
        if(DispoPeriodDays.fromObject(perioObj).isAvailable()) result.push(user);
      }
    });    
    return result;
  }
}
