import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBoxUnusedPeriodComponent } from './modal-box-unused-period.component';

describe('ModalBoxUnusedPeriodComponent', () => {
  let component: ModalBoxUnusedPeriodComponent;
  let fixture: ComponentFixture<ModalBoxUnusedPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBoxUnusedPeriodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBoxUnusedPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
