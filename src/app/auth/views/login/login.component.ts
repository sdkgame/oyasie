import { Component, OnInit } from '@angular/core';
import {EmailValidator, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { User } from 'src/app/modele/user';
import { Router } from '@angular/router';
import { CustomFormValidators } from './../../shared/form/validators/CustomFormValidators';
import { ActionStatut } from 'src/app/modele/statut';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  childs : any[]=[
    {
      id:'emailConnexion',
      icon:'@',
      type:'email',
      placeholder:'email',
      name:'email',
      span:{
        class:'emailExample',
        text:'Ex:toto@gmail.com'
      },
      class:'',
      validators:[Validators.required,CustomFormValidators.email]
    },
    {
      id:'password',
      name:'password',
      icon:'<i class="fa fa-key"></i>',
      type:'password',
      placeholder:'Mot de passe',
      class: 'second-input-group',
      validators:[Validators.required,Validators.minLength(4)]
    }
  ];
  textBtn:string="Connexion";
  textError:string="";
  textWaiting:string="Veuillez patienter...";
  constructor(private authService: AuthService,private router:Router) { }

  ngOnInit(): void {}
  submit(data)
  {
    this.textError="Veuillez patienter..";
    this.authService.signIn(new User(data.email,data.password))
    .then(result=>{
      this.router.navigate(["dashboard/home"]);
    })
    .catch((e)=>{
      this.textError=e.message;
      console.log(e);
    });
  }

}
