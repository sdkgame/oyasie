import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavAccueilComponent } from './nav-accueil.component';

describe('NavAccueilComponent', () => {
  let component: NavAccueilComponent;
  let fixture: ComponentFixture<NavAccueilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavAccueilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavAccueilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
